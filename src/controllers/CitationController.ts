import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class CitationController
{
    static index(req: Request, res: Response): void
    {
        const id = +req.params.id
        let offset = (id-1)*10

        let page = db.prepare('SELECT COUNT(*) FROM citations').all()
        let citation = db.prepare('SELECT * FROM citations LIMIT 10 OFFSET ?').all(offset)

        console.log(offset);
        console.log(id);
        console.log(page);

        const table_parse = Number(Math.round(page[0]['COUNT(*)'] / 10))

        // On fait un array pour pouvoir proposer un nombre de pages dynamiques
        let tableau_count:any = []
        let i = 0
        while(i<table_parse){
            i++
            tableau_count.push(i)
        }   
        
        console.log(tableau_count);
        console.log(table_parse);
        console.log(page[0]['COUNT(*)']);
        
        
        
        res.render('pages/citations', {
            title: 'Welcome',
            citation: citation,
            id: tableau_count,
        });
    }
}