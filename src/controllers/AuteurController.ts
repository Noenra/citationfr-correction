import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class AuteurController
{
    static index(req: Request, res: Response): void
    {
        const table: any = []
        const auteurCitation: any = []

        const citation = db.prepare('SELECT auteurs_id FROM citations').all()
        const auteur = db.prepare('SELECT * FROM auteurs').all()

        citation.forEach((elem: any) => {
            table.push(elem.auteurs_id)
        })
        auteur.forEach((elem: any) => {
            if(table.includes(Number(elem.id))){
                auteurCitation.push(elem)
            }
        })
        
        console.log(auteurCitation);
        console.log(table);
        

        res.render('pages/auteurs', {
            title: 'Auteurs',
            auteur_citation: auteurCitation,
        });
    }

    static auteurCitation(req: Request, res: Response): void
    {
        const auteurCitations = db.prepare('SELECT * FROM citations WHERE auteurs_id = ?').all(req.params.id)
        const auteurName = db.prepare('SELECT name FROM auteurs WHERE id = ?').get(req.params.id)
        
        res.render('pages/auteur-read', {
            auteur: auteurName,
            citations: auteurCitations
            
        });
    }
}