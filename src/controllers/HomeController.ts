import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const lastCitation = db.prepare('SELECT * FROM citations INNER JOIN auteurs ON citations.auteurs_id = auteurs.id ORDER BY citations.id DESC').get()
        console.log(lastCitation);
        
        res.render('pages/index', {
            title: 'Welcome',
            citation: lastCitation,
        });
    }
}