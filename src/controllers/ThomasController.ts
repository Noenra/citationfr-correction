import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class ThomasController
{
    static index(req: Request, res: Response): void
    {
        const auteur = db.prepare('SELECT * FROM auteurs').all()
        res.render('pages/backofice', {
            title: 'Welcome',
            auteur: auteur,
        });
    }

    static editAuteur(req: Request, res: Response): void
    {
        const auteur = db.prepare('SELECT * FROM auteurs WHERE id = ?').get(req.params.id);
        res.render('pages/auteur-edit', {
            title: auteur,
        })
    }

    static updateAuteur(req: Request, res: Response): void
    {
        db.prepare('UPDATE auteurs SET name = ? WHERE id = ?').run(req.body.name, req.params.id);
        res.redirect('/admin');
    }

    static deleteAuteur(req: Request, res: Response): void
    {
        db.prepare('DELETE FROM citations WHERE auteurs_id = ?').run(req.params.id)
        db.prepare('DELETE FROM auteurs WHERE id = ?').run(req.params.id)
        res.redirect('/admin');
    }

    static createAuteur(req: Request, res: Response): void
    {
        db.prepare('INSERT INTO auteurs ("name") VALUES (?)').run(req.body.createAuteur)
        res.redirect('/admin');
    }
}