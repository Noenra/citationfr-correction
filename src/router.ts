import { Application } from "express";
import HomeController from "./controllers/HomeController";
import AuteurController from "./controllers/AuteurController";
import CitationController from "./controllers/CitationController";
import ThomasController from "./controllers/ThomasController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });
    
    app.get('/auteurs', (req, res) =>
    {
        AuteurController.index(req, res);
    });

    app.get('/auteurs/:id', (req, res) =>
    {
        AuteurController.auteurCitation(req, res);
    });

    app.get('/citations/:id', (req, res) =>
    {
        CitationController.index(req, res);
    });

    app.get('/admin', (req, res) =>
    {
        ThomasController.index(req, res);
    });

    app.get('/admin/auteurs/edit/:id', (req, res) =>
    {
        ThomasController.editAuteur(req, res);
    });

    app.post('/admin/auteurs/edit/:id', (req, res) =>
    {
        ThomasController.updateAuteur(req, res);
    });

    app.get('/admin/auteurs/delete/:id', (req, res) =>
    {
        ThomasController.deleteAuteur(req, res);
    });

    app.post('/admin', (req, res) =>
    {
        ThomasController.createAuteur(req, res);
    });
}
