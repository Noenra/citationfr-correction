-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Dylan Benchalal--Galopin
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-24 09:18
-- Created:       2022-02-24 09:18
PRAGMA foreign_keys = OFF;


BEGIN;
CREATE TABLE "auteurs"(
  "id" INTEGER PRIMARY KEY NOT NULL,
  "name" VARCHAR(45) NOT NULL
);
CREATE TABLE "citations"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" VARCHAR(255) NOT NULL,
  "auteurs_id" INTEGER NOT NULL,
  CONSTRAINT "fk_citations_auteurs"
    FOREIGN KEY("auteurs_id")
    REFERENCES "auteurs"("id")
);
CREATE INDEX "citations.fk_citations_auteurs_idx" ON "citations" ("auteurs_id");
COMMIT;
